<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" type="text/css" href="../styles/styles.css">
    <title>ecomail</title>
</head>

<body class="body">
    <header class="header">
        <section class="firstheader-section">
            <img src="../styles/assets/image/account1.png" alt="icon" class="firstheader-imageaccount-section">
            <p class="firstheader-account-section">lorem@gmail.com</p>
            <button class="firstheader-addacount-section" id="addacount">▽</button>
            <img src="../styles/assets/image/search1.png" alt="iconsearch" class="firstheader-search-section"
                id="search">
        </section>
        <section class="secondheader-section">
            <ul class="secondheader-menu-section">
                <li class="iconmenu"><a href="./home.php"><img src="../styles/assets/image/iconmenu1.png" alt="iconmenu"></a></li>
                <li class="iconmenu"><a href="./notif.php"><img src="../styles/assets/image/iconmenu2.png" alt="iconmenu"></a></li>
                <li class="iconmenu"><a href="./promo.php"><img src="../styles/assets/image/iconmenu3.png" alt="iconmenu"></a></li>
                <li class="iconmenu"><a href="./dossier.php"><img src="../styles/assets/image/iconmenu4.png" alt="iconmenu"></a></li>
                <li class="iconmenu"><a href="./setting.php"><img src="../styles/assets/image/iconmenu5.png" alt="iconmenu"></a></li>
            </ul>
        </section>
    </header>